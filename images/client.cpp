/*
 *   file:   client.cpp
 *   Simple sockets client
**/
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>

#include <opencv2/opencv.hpp>

/*
 * Variables
**/
/* Client can connect to a server either on the local machine or across the network. */
#define PORT 4201
#define LOCAL "127.0.0.1"
#define REMOTE "192.168.1.50"
#define BUFLEN 30
#define WIDTH 640
#define HEIGHT 480
#define DEPTH 1
#define CHANNEL 3

int client_socket;
char buffer[BUFLEN];
unsigned int compteur = 0;
int displaytime = 10 ; /* in ms*/
cv::Mat image = cv::Mat(HEIGHT, WIDTH, CV_8UC3) ;	

/*
* Handler of timer
*/
void handler_signal(int no) {
    int len;
	printf("%d\n", ++compteur); printf("from Client : ");
	strcpy(buffer, "Request image\n");
	send(client_socket, buffer, BUFLEN, 0); 
	len = recv(client_socket, image.data, WIDTH*HEIGHT*DEPTH*CHANNEL, 0);
	buffer[len] = 0;		/* last char is zero*/
	printf ("Server << "); puts (buffer);	

	cv::imshow ("Frame client", image) ;
	cv::waitKey(displaytime);
}

int timer_init(void) {
    timer_t timer = (timer_t) 0;
    long int periode = 70 ;	/* la période doit être en ms: below 70 ms it does not work */
    struct sigevent event;
    struct itimerspec spec;

	// Configurer le timer
	signal(SIGRTMIN, handler_signal);
	event.sigev_notify = SIGEV_SIGNAL;
	event.sigev_signo  = SIGRTMIN;
	periode = periode*1000000 ;	/* conversion en ns */
	spec.it_interval.tv_sec  = periode/1000000000;
	spec.it_interval.tv_nsec = periode % 1000000000;	
	spec.it_value = spec.it_interval;

	// Allouer le timer
	if (timer_create(CLOCK_REALTIME, &event, &timer) != 0) {
		perror("timer_create");
		exit(EXIT_FAILURE);
	} 

	// Programmer le timer
	if (timer_settime(timer, 0, &spec, NULL) != 0) {
		perror("timer_settime");
		exit(EXIT_FAILURE);
	}
    return 0;
}

/*
* Main thread
*/
int main (int argc, char *argv[]) {
    int result;
    struct sockaddr_in client_addr;
    const char *server = LOCAL ;
    if (argc > 1)
        if (strcmp (argv[1], "remote") == 0)
            server = REMOTE ;

    /* Create unnamed socket and then name it */
    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    client_addr.sin_family = AF_INET;
    result = inet_aton(server, &client_addr.sin_addr);
	/* inet_aton: convert IP addresses from dots-and-number to abinary form and stores it in structure */
    if (result == 0) {
        perror("inet_aton error");
        exit(errno);
    }
    client_addr.sin_port = htons(PORT);

    /* Connect to the server */
    result = connect(client_socket, (struct sockaddr *) &client_addr, sizeof(client_addr));
    if (result < 0) {
        perror("Client can't connect");
        exit(errno);
    }

	
	while (1)
		pause() ;
	
	cv::destroyAllWindows() ;
    return 0;
}

