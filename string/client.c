/*
    file:   client.c
    Simple sockets client
*/
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
// timer libs
#include <time.h>
#include <sys/time.h>
#include <signal.h>

/*
    Variables
*/
/* Client can connect to a server either on the local machine or across the network. */
#define LOCAL "127.0.0.1"
#define REMOTE "192.168.1.50"
#define BUFLEN 80
#define PORT 4201

/*
 * Timer handler
 */
void handler_signal (int inutilise) {
	fprintf(stderr, "1 ");
}

/*
    Main thread
*/
int main (int argc, char *argv[]) {
    int client_socket;
    struct sockaddr_in client_addr;
    char ch, text[BUFLEN], msg[BUFLEN] = "TEST REQUEST\r\n\0";
    char *server = LOCAL;
    int result, len;

    /*
     * TIMER
     */
	timer_t timer;
	struct sigevent event;
	struct itimerspec spec;
    
    // Handler
	signal(SIGRTMIN+1, handler_signal);
	// Error notification
	event.sigev_notify = SIGEV_SIGNAL;
	event.sigev_signo  = SIGRTMIN+1;
	// Period configuration
	spec.it_interval.tv_sec  = 0;
	spec.it_interval.tv_nsec = 10000000; // 10ms
	spec.it_value = spec.it_interval;
	// Allocate
	if (timer_create(CLOCK_REALTIME, &event, &timer) != 0) {
		perror("timer_create");
		exit(EXIT_FAILURE);
	} 
	// Launch the timer
	if ((timer_settime(timer, 0, &spec, NULL) != 0)) {
		perror("timer_settime");
		exit(EXIT_FAILURE);
	}

    /*
     * Connect to the server
     */
    if (argc > 1)
        if (strcmp(argv[1], "remote") == 0)
            server = REMOTE;

	/* Create unnamed socket and then name it */
    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    client_addr.sin_family = AF_INET;
    result = inet_aton(server, &client_addr.sin_addr);
	/* inet_aton: convert IP addresses from dots-and-number to abinary form and stores it in structure */
    if (result == 0) {
        perror("inet_aton error");
        exit(errno);
    }
    client_addr.sin_port = htons(PORT);

	/* Connect to the server */
    result = connect(client_socket, (struct sockaddr *) &client_addr, sizeof (client_addr));
    if (result < 0) {
        perror("Client can't connect");
        exit(errno);
    }
	
    while (1) {
        pause(); // wait for the timer
        /* Connection established.  Transaction */
        do {
            printf("from Client: ");
            puts(msg);
            //fgets(text, BUFLEN, stdin);
            ch = text[0];	/* save first char */
            send(client_socket, msg, strlen(msg), 0);
            len = recv(client_socket, text, sizeof text-1, 0);		/* last byte for zero*/
            text[len] = 0;		/* last char is zero*/
            printf("from Server: ");
            puts(text);
        } while (ch != 'q');
    }
    close(client_socket);
	printf("Connection terminated.  Client shutting down\n");
    return 0;
}

