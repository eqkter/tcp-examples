/*
* file:   server.cpp
* Simple sockets server
*/
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <opencv2/opencv.hpp>

/*
* Variables
*/
#define PORT 4201
#define SERVER "127.0.0.1"
#define BUFLEN 30
#define WIDTH 640
#define HEIGHT 480
#define DEPTH 1
#define CHANNEL 3

/*
* Main thread
*/
int main (void) {
    int server_socket, client_socket, client_len;
    struct sockaddr_in server_addr, client_addr;
    char buffer[BUFLEN];
    int len, result;
    cv::Mat image = cv::Mat(HEIGHT, WIDTH, CV_8UC3) ;	/* Allocate a 3-channel WIDTH x HEIGHT image */

	/* Test image acquisition */
	cv::VideoCapture vcap(0);	/* Data structure for image acquisition */
    if (!vcap.isOpened()) {
        printf("Error opening camera \n");
        exit(errno) ;
    }
	
	/*Create unnamed socket and give it a "name" */
    server_socket = socket(PF_INET, SOCK_STREAM, 0);
    server_addr.sin_family = AF_INET;
    result = inet_aton(SERVER, &server_addr.sin_addr);
	/* inet_aton: convert IP addresses from dots-and-number to abinary form and stores it in structure */
    if (result == 0) {
        printf("inet_aton failed\n");
        exit(errno);
    }
    server_addr.sin_port = htons(PORT);

	/* Bind to the socket */
    result = bind(server_socket, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (result != 0) {
        perror("bind");
        exit(errno);
    }

	/* Create a client queue */
    result = listen(server_socket, 1);
    if (result != 0) {
        perror("listen");
        exit(errno);
    }
    printf("Network server running\n");

	/* Accept a connection */
    client_len = sizeof(client_addr);
    client_socket = accept(server_socket, (struct sockaddr *) &client_addr, (socklen_t * __restrict__) &client_len);

    printf("Connection established to %s\n", inet_ntoa(client_addr.sin_addr));
	/* inet_ntoa: convert IP addresses from dots-and-number to a struct in_addr and back */

    do {
		printf("from Client: ");
		len = recv(client_socket, buffer, BUFLEN, 0);	/* last byte for zero*/
		puts(buffer);
		printf("from Server: Send image\n ");
		vcap >> image ;
		usleep(40000) ;	/* below 40 ms it does not work*/
        send(client_socket, image.data, WIDTH*HEIGHT*DEPTH*CHANNEL, 0) ; 
    } while (1);

    close(client_socket);
    close(server_socket);
    printf ("Connection terminated.  Server shutting down\n") ;
	vcap.release();
    return 0;
}
